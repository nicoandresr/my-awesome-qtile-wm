#                                                                           
#      ,o888888o. 8888888 8888888888  8 8888 8 8888         8 8888888888   
#   . 8888     `88.     8 8888        8 8888 8 8888         8 8888         
#  ,8 8888       `8b    8 8888        8 8888 8 8888         8 8888         
#  88 8888        `8b   8 8888        8 8888 8 8888         8 8888         
#  88 8888         88   8 8888        8 8888 8 8888         8 888888888888 
#  88 8888     `8. 88   8 8888        8 8888 8 8888         8 8888         
#  88 8888      `8,8P   8 8888        8 8888 8 8888         8 8888         
#  `8 8888       ;8P    8 8888        8 8888 8 8888         8 8888         
#   ` 8888     ,88'8.   8 8888        8 8888 8 8888         8 8888         
#      `8888888P'  `8.  8 8888        8 8888 8 888888888888 8 888888888888 
#
# This is my awesowe qtile config.

from typing import List
from libqtile import bar, layout, widget
from libqtile.config import Click, Drag, Group, Key, Match, Screen
from libqtile.lazy import lazy
from libqtile.utils import guess_terminal

mod = "mod4"
terminal = guess_terminal()
firefox = "firefox-developer-edition"
chromium = "chromium"
thunderbird = "thunderbird"

keys = [
    # Switch between windows dvorak motions.
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "s", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "t", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "n", lazy.layout.up(), desc="Move focus up"),
    Key([mod], "space", lazy.layout.next(), desc="Move window focus to other window"),

    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"], "h", lazy.layout.shuffle_left(), desc="Move window to the left"),
    Key([mod, "shift"], "s", lazy.layout.shuffle_right(), desc="Move window to the right"),
    Key([mod, "shift"], "t", lazy.layout.shuffle_down(), desc="Move window down"),
    Key([mod, "shift"], "n", lazy.layout.shuffle_up(), desc="Move window up"),

    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"], "h", lazy.layout.grow_left(), desc="Grow window to the left"),
    Key([mod, "control"], "s", lazy.layout.grow_right(), desc="Grow window to the right"),
    Key([mod, "control"], "t", lazy.layout.grow_down(), desc="Grow window down"),
    Key([mod, "control"], "n", lazy.layout.grow_up(), desc="Grow window up"),

    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config",),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile",),

    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),

    # Toggle between last visited group
    Key([mod], "d", lazy.screen.toggle_group(), desc="Toggle between groups"),
    Key([mod], "w", lazy.window.kill(), desc="Kill focused window"),

    Key([mod], "r", lazy.spawncmd(), desc="Spawn a command"),
    Key([mod], "f", lazy.spawn(firefox), desc="Spawn firefox"),
    Key([mod], "t", lazy.spawn(thunderbird), desc="Spawn thunderbird"),
    Key([mod], "s", lazy.spawn('flatpak run com.slack.Slack'), desc="Spawn slack"),
    Key([mod], "v", lazy.spawn(chromium), desc="Spawn chromium"),
    Key([mod], "z", lazy.window.move_down()),
    # Toggle minimize and bring window to front
    Key([mod], "m", lazy.window.toggle_minimize(), desc="Toggle minimize and bring window to front"),
]

groups = [
    Group(""),
    Group("", matches=[Match(wm_class=["Navigator", "firefoxdeveloperedition"])]),
    Group(""),
    Group("", matches=[Match(wm_class=["thunderbird"])]),
    Group(""),
    Group("", matches=[Match(wm_class=["Cypress", "cyrpess"])]),
]

for count, group in enumerate(groups, start=1):
    keys.extend([
        # mod1 + letter of group = switch to group
        Key([], f"F{count}", lazy.group[group.name].toscreen(),
            desc="Switch to group {}".format(group.name)),

        # mod1 + shift + letter of group = switch to & move focused window to group
        Key(["shift"], f"F{count}", lazy.window.togroup(group.name, switch_group=False),
            desc="No switch to & move focused window to group {}".format(group.name)),
    ])

#  groups = [
#      Group("", layout="max",        matches=[Match(wm_class=["navigator", "firefox", "vivaldi-stable", "chromium", "brave"])]),
#      Group("", layout="max",        matches=[Match(wm_class=["navigator", "firefox", "vivaldi-stable", "chromium", "brave"])]),
#      Group("", layout="monadtall",  matches=[Match(wm_class=["emacs", "geany", "subl"])]),
#      Group("", layout="monadtall",  matches=[Match(wm_class=["inkscape", "nomacs", "ristretto", "nitrogen"])]),
#      Group("", layout="monadtall",  matches=[Match(wm_class=["qpdfview", "thunar", "nemo", "caja", "pcmanfm"])]),
#      Group("", layout="max",        matches=[Match(wm_class=["telegramDesktop"])]),
#      Group("", layout="ratiotile"),
#      Group("", layout="max",        matches=[Match(wm_class=["spotify", "pragha", "clementine", "deadbeef", "audacious"]), Match(title=["VLC media player"])]),
#      Group("", layout="tile"),
#  ]

#  for k, group in zip(["1", "2", "3", "4", "5", "6", "7", "8"], groups):
#      keys.append(Key("M-"+(k), lazy.group[group.name].toscreen()))
#      keys.append(Key("M-S-"+(k), lazy.window.togroup(group.name)))

#  groups = [Group(i) for i in "123456789"]

colors = []
cache='/home/nicoandres/.cache/wal/colors'
def load_colors(cache):
    with open(cache, 'r') as file:
        for i in range(8):
            colors.append(file.readline().strip())
    colors.append('#ffffff')
    lazy.reload()
load_colors(cache)

layouts = [
    layout.Columns(border_focus_stack=[colors[0], colors[1]], border_width=5, margin=0),
    layout.Max(),
    layout.Floating(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    #layout.MonadTall(ratio=0.65, border_focus=colors[0], margin=0),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(
    font="VictorMono Nerd Font",
    fontsize=24,
    padding=6,
)

extension_defaults = widget_defaults.copy()

screens = [
    Screen(
        # right=bar.Gap(270),
        #  top=bar.Bar(
        #      [
        #          #widget.CurrentLayout(),
        #          widget.Image(filename='~/Images/Icons/Riise.png'),
        #          widget.GroupBox(),
        #          widget.Prompt(),
        #          widget.Spacer(),
        #          widget.WindowName(),
        #          widget.Chord(
        #              chords_colors={
        #                  'launch': ("#ff0000", "#ffffff"),
        #              },
        #              name_transform=lambda name: name.upper(),
        #          ),
        #          #widget.TextBox("default config", name="default"),
        #          #widget.TextBox("Press &lt;M-r&gt; to spawn", foreground="#d75f5f"),
        #          widget.Systray(),
        #          #widget.Wttr('Helsinki: %t(%f), Moon: %m', location={'Helsinki': 'Helsinki'}),
        #          #widget.Pomodoro(),
        #          widget.Clock(format='%m-%d-%Y %a %I:%M %p'),
        #          #PowerlineTextBox(font='FiraCode Nerd Font'),
        #          widget.QuickExit(),
        #      ],
        #      36,
        #  ),
    ),
    Screen(
        # right=bar.Gap(260),
    ),
]

# Drag floating layouts.
mouse = [
    Drag([mod], "Button1", lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod], "Button3", lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front())
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: List
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class='confirmreset'),  # gitk
    Match(wm_class='makebranch'),  # gitk
    Match(wm_class='maketag'),  # gitk
    Match(wm_class='ssh-askpass'),  # ssh-askpass
    Match(title='branchdialog'),  # gitk
    Match(title='pinentry'),  # GPG key password entry
    Match(wm_class='pinentry-gtk'), # GPG key password
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = True

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"

floats_kept_above = False
